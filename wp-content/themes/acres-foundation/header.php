<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Acres_Foundation
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div class="navigation-wrap start-header start-style">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 max-width-ct">
					<nav class="navbar navbar-expand-md navbar-light">
					
						<a class="navbar-brand" href="<?php echo site_url(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/logo.svg" alt=""></a>	
						
						<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topmenu" aria-controls="topmenu" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button> -->
						<button type="button" data-toggle="modal" data-target="#sidebar-right" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>
						
						<div class="collapse navbar-collapse topmenu">
							<ul class="navbar-nav ml-auto py-4 py-md-0">
								<!-- <li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li> -->
								<li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link" href="<?php echo site_url(); ?>">Home</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">LEADERSHIP</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Approach</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">PARTNERSHIPS</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">BLOG</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Contact</a>
								</li>
								<li  class="nav-item pl-md-0 ml-0">
									<a href="" class="nav-link">
										<h2>DOWNLOAD Partnership Info</h2>
									</a>
								</li>
							</ul>
						</div>
						
					</nav>		
				</div>
			</div>
		</div>
	</div>


	<!-- mobile menu -->
<div class="modal fade right" id="sidebar-right" tabindex="-1" role="dialog">
<div class="modal-dialog modal-sm" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
<!-- <h4 class="modal-title">Right Sidebar</h4> -->
</div>
<div class="modal-body p-0">
<div class="topmenu">
							<ul class="navbar-nav ml-auto py-4 py-md-0">
								<li class="nav-item pl-md-0 ml-0 active">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Portfolio</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Agency</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Services</a>
									<div class="dropdown-menu">
										<a class="dropdown-item" href="#">Action</a>
										<a class="dropdown-item" href="#">Another action</a>
										<a class="dropdown-item" href="#">Something else here</a>
										<a class="dropdown-item" href="#">Another action</a>
									</div>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Journal</a>
								</li>
								<li class="nav-item pl-md-0 ml-0">
									<a class="nav-link" href="#">Contact</a>
								</li>
							</ul>
							<a href="" class="mob-down">
										<h2>DOWNLOAD Partnership Info</h2>
									</a>
						</div>

</div>
</div>
</div>
</div>
