<?php /* Template Name: Approach page */ ?>
<?php get_header(); ?>
<section class="max-width-ct">
	<div class="container-fluid pt-4">
		<div class="row">
			<div class="col top-slider">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Approach-Header.jpg" alt="" class="img-fluid">
				<div class="sty-ribban">
					<h2>Bringing the best ideas</h2>
					<p>in global education to India.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="comman-cls max-width-ct">
	<div class="container-fluid">
		<div class="row leadership-ct approaches-ct">
			<div class="col-md-6">
				<h2 class="pb-0">A complete ecosystem</h2>
				<h5>to create world leading education.</h5>
				<p>A highly advanced and interconnected system of AF Research & Innovation Centre, AF Leadership & Education Institute and AF Schools at The Acres Foundation gives us the edge.</p>
<p>In this unique ecosystem, a think tank develops pioneering educational approaches that work for the Indian Context, and then teachers are trained with these methods before implementing in schools. The data and feedback received from the schools are then analysed and evaluated.</p>
			</div>
			<div class="col-md-6 col">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/approach-sc.png" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row model-school-ct">
			<h2>The Acres Foundation<br>School Model</h2>
			<p>Designed to create leading edge institutions that bring the best of global education and work for the Indian context.</p>

			<div class="inner-sh-ct">
				<h3>Ready-To-Use Content & Curriculum</h3>
				<h4>LEAPED®</h4>
				<p>‘Leadership, Ethics and Awareness of Personal & Emotional Development’ is a proprietary program that binds together our curriculum and differentiates our children from the rest, and set them on the path to be true CHANGEMAKERS.</p>

				<h4>HOMEROOM®</h4>
				<p>A proprietary daily program that teaches kids socio-emotional skills.</p>

				<h4>YOGIJr®</h4>
				<p>A mindfulness & meditation program to help our children focus their attention, as well as observe and understand their thoughts and feelings.</p>

				<h4>Curated Curriculum</h4>
				<p>Selecting from the best national and international curricular resources, we create a powerful mix of online and offline learning that’s tailor-made and ready-made.</p>
			</div>

			<div class="inner-sh-ct">
				<h3>Teacher-Leader Recruitment & Development</h3>
				<ul>
					<li>Our 4-step Selection Process – Pre-screening, Psychometric Testing, Live Testing and Selection – to ensure we attract and hire the right talent.</li>
					<li>Teachers and Leaders have access to all resources and programs from The Acres Foundation Leadership & Education Institute (such as LEADed, AF Way, RightStart, Super Saturdays, and PLCs).</li>
					<li>AF Proprietary and digital based Performance Management System (PMS) and Personal Improvement Planning (PiP) system.</li>
				</ul>
			</div>

			<div class="inner-sh-ct">
				<h3>Safe & Conducive Learning Environment</h3>
				<ul>
					<li>School-in-a-box® checklists to ensure clean, safe, and secure buildings.</li>
					<li>School-in-a-box® SOPs for all administrative processes to ensure clear and simple administration.</li>
					<li>Multi-tier System of Supports (MTSS) Program to support inclusion and learning differences.</li>
					<li>Positive Behavioural Interventions and Supports (PBIS) Program to improve and integrate all of the data, systems, and practices, affecting student outcomes every day.</li>
				</ul>
			</div>

			<div class="inner-sh-ct">
				<h3>Continuous Monitoring & Improvement</h3>
				<p>A year-long format for measuring performance across all school functions, analysing the data, providing feedback and support to team members, and continuously improving each department, school, and our organisation.</p>
			</div>

			<div class="inner-sh-ct">
				<h3>Budget Driven Management</h3>
				<ul>
					<li>Budgeting cycle from 1st November to 31st January.</li>
					<li>Software tracks spend vs. budget on a daily basis to ensure smooth controls.</li>
					<li>Spending capped as a percentage of fees (i.e. 40-50% of fees go to program, the rest to property, intellectual capital & management).</li>
				</ul>
			</div>

			<div class="inner-sh-ct">
				<h3>Parent & Community Engagement</h3>
				<ul>
					<li>Online and offline ‘parent workshops’ by a variety of experts selected by our network.</li>
					<li>Feedback and survey processes throughout the year to engage parents in meaningful ways.</li>
					<li>Unique PTA and Committee engagement plan plays a vital role in enhancing and enriching the lives of the students.</li>
				</ul>

				<div class="container">
					<div class="row first-act">
						<div class="col-sm-3 col-4 col-md-3 red-bg">
							<h2>ACT</h2>
							<ul>
								<li>Adopt, adapt or abandon cycle</li>
								<li>If adopting with no change, roll out with improvement</li>
							</ul>
						</div>
						<div class="col-sm-3 col-4 col-md-3 right-arrow-ct">
							<img src="<?php echo get_template_directory_uri()  ?>/assets/images/arrow.svg" alt="" class="img-fluid">
						</div>
						<div class="col-sm-3 col-4 col-md-3 purple-bg">
							<h2>PLAN</h2>
							<ul>
								<li>Set improvement goals</li>
								<li>Predict what will happen</li>
								<li>Plan the cycle (who, where, what and how)</li>
								<li>Decide what data to gather</li>
							</ul>
						</div>
					</div><!-- first row close -->
					<div class="row">
						<div class="col-sm-3 col-4 col-md-3 up-arrow-ct">
							<img src="<?php echo get_template_directory_uri()  ?>/assets/images/arrow.svg" alt="" class="img-fluid">
						</div>
						<div class="col-sm-3 col-4 col-md-3 middle-ma-text">
							<h3>Continuous<br>Improvement®</h3>
						</div>
						<div class="col-sm-3 col-4 col-md-3 down-arrow-ct">
							<img src="<?php echo get_template_directory_uri()  ?>/assets/images/arrow.svg" alt="" class="img-fluid">
						</div>
					</div><!-- second row close -->
					<div class="row first-act">
						<div class="col-sm-3 col-4 col-md-3 purple-bg">
							<h2>STUDY</h2>
							<ul>
								<li>Fully analyse</li>
								<li>Compare data predictions</li>
								<li>Examine learning</li>
							</ul>
						</div>
						<div class="col-sm-3 col-4 col-md-3 left-arrow-ct">
							<img src="<?php echo get_template_directory_uri()  ?>/assets/images/arrow.svg" alt="" class="img-fluid">
						</div>
						<div class="col-sm-3 col-4 col-md-3 red-bg">
							<h2>DO</h2>
							<ul>
								<li>Carry out plan</li>
								<li>Document any problems encountered and observations</li>
								<li>Gather data</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="vision-ct">
	<h2>Our motto</h2>
	<div class="blockqute-ct">
		<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blolqu.svg" alt="" class="img-fluid">
		<p>If you are not moving forward,<br>you are falling behind.</p>
		<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blolqu1.svg" alt="" class="img-fluid">
	</div>
</section>




<section>
	<div class="container">
		<div class="row model-school-ct">
			<h2>The Acres Foundation<br>Research & Innovation Centre</h2>
			<p>Bringing together a team of academics from the world’s best universities, such as Harvard, Columbia and Johns Hopkins, we have created a leading-edge educational think tank. To provide the best of global education relevant to India.</p>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row model-school-ct">
			<h2>The Acres Foundation<br>Leadership & Education Institute</h2>
			<p>Teaching is an evolving skill. And teachers, who are thought leaders, never stop learning. Working closely with the Research and Innovation Centre, our teachers are continually upskilling and learning the latest teaching theories and techniques.</p>

			<div class="inner-sh-ct">
				<h3>LeadED® – Our Continuous Leadership in Education Program</h3>
				<p>Teacher training and development courses ranging between 180 – 220 hours annually, include:</p>

				<h4>The AF Way®</h4>
				<p>Development course for all new hires.</p>

				<h4>RightStart®</h4>
				<p>Summer development course before the start of academic year.</p>

				<h4>Super Saturdays®</h4>
				<p>Year long fortnightly development program with modules on behaviour, pedagogy skills, lesson planning, etc.</p>

				<h4>PLCs®</h4>
				<p>Year long Professional Learning Communities.</p>

				<h4>Mentoring</h4>
				<p>AF mentors continually.</p>
			</div>
		</div>
	</div>
</section>











<style type="text/css">




.button:hover {
  /*background: #06D85F;*/
}


.overlay:target {
 /* visibility: visible;
  opacity: 1;*/
}

.popup .close:hover {
  /*color: #06D85F;*/
}
</style>
<?php get_footer(); ?>