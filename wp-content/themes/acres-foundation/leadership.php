<?php /* Template Name: Leadership page */ ?>
<?php get_header(); ?>

<section class="comman-cls max-width-ct">
	<div class="container-fluid">
		<div class="row leadership-ct">
			<div class="col-md-6">
				<h2>The finest minds in education.</h2>
				<p>When you have academics from top global institutions as one of your greatest assets, you have the opportunity to create a school that’s not only successful but also trailblazing.</p>
			</div>
			<div class="col-md-6 col">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Leadership.jpg" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>
<section class="comman-cls">
	<div class="container">
		<div class="row board-members-ct">
			<h2>The Acres Foundation</h2>
			<p>Board Members</p>
		</div>

		<div class="row members-ct">
			<ul>
				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Rohan-Parikh.jpg" alt="" class="img-fluid">
					<h3>Rohan Parikh</h3>
					<p>M. Ed., Johns Hopkins University<br>
MBA, INSEAD<br>
B. Sc. Econ., The Wharton School of Business Managing<br>
Director, The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Siamek-Zahedi.jpg" alt="" class="img-fluid">
					<h3>Dr. Siamack Zahedi</h3>
					<p>Doctorate in Education,<br>
Johns Hopkins University<br>
M. Ed. Columbia University<br>
Co-CEO and Director of Education & Research,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Romil-Parikh.jpg" alt="" class="img-fluid">
					<h3>Romil Parikh</h3>
					<p>B.Sc., Indiana University<br>
Joint Managing Director, The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Aditya-Lohana.jpg" alt="" class="img-fluid">
					<h3>Prof. Aditya Lohana</h3>
					<p>F.C.A., L.L.B. (Gen.)<br>
Ex-Faculty in Finance for Undergrad students at<br>
Mithibai college, Mumbai<br>
Founder, Lohana’s Test Series<br>
Executive Director,<br>
The Acres Foundation</p>
				</li>


				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Tejas-Parekh.jpg" alt="" class="img-fluid">
					<h3>Tejas Parekh</h3>
					<p>Masters in Marketing & Management,<br>
Southampton School<br>
Co-CEO and Director of Operations,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Radhika-Zahedi.jpg" alt="" class="img-fluid">
					<h3>Radhika Zahedi</h3>
					<p>Masters in Mathematics Education,<br>
Teachers College,<br>
Columbia University, USA<br>
Bachelor’s Degree in Engineering,<br>
Mumbai University<br>
School Director, The Green Acres Academy Chembur</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Ruchita-Chopra.jpg" alt="" class="img-fluid">
					<h3>Ruchita Chopra</h3>
					<p>Masters in Psychology from IGNOU<br>
Diversity & Inclusion Program,<br>
Purdue University<br>
Program in School Management & Leadership Application, Harvard University<br>
Vice President, Human Resources,<br>
The Acres Foundation</p>
				</li>
			</ul>
		</div>
	</div>
</section>

<section class="comman-cls">
	<div class="container">
		<div class="row board-members-ct">
			<h2>The Acres Foundation</h2>
			<p>Team Members</p>
		</div>
		<div class="row members-ct">
			<ul>
				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Anuj-Iyer.jpg" alt="" class="img-fluid">
					<h3>Anuj Iyer</h3>
					<p>M. Ed. Harvard University<br>
Head of Learning & Innovations,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Sanyukta-Bafna.jpg" alt="" class="img-fluid">
					<h3>Sanyukta Bafna</h3>
					<p>M. Ed., Teachers College<br>
(Specialization in Curriculum & Teaching),<br>
Columbia University<br>
Head of Citizenship Program,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Pooja-Harlalka.jpg" alt="" class="img-fluid">
					<h3>Pooja Harlalka</h3>
					<p>Licensed Mental Health Counselor<br>
M.Ed., Teachers College, Columbia University<br>
M.A., Teachers College, Columbia University<br>
Head of Special Education Needs,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Smriti-Parikh.jpg" alt="" class="img-fluid">
					<h3>Smriti Parikh</h3>
					<p>Masters in Education,<br>
Harvard Graduate School of Education (Learning and Teaching)<br>
Head of Literacy Programs,<br>
The Acres Foundation</p>
				</li>

				<li>
					<img src="<?php echo get_template_directory_uri()  ?>/assets/images/Mehernaz-Eshraghi.jpg" alt="" class="img-fluid">
					<h3>Mehernaz Eshraghi</h3>
					<p>M. Ed., Harvard University<br>
M. Ed., Language and Literacy<br>
Instructional Head,<br>
The Green Acres Academy, Mulund</p>
				</li>
			</ul>
		</div>
	</div>
</section>
<?php get_footer(); ?>