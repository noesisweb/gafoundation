<?php /* Template Name: Partnership page */ ?>
<?php get_header(); ?>
<section class="comman-cls max-width-ct">
	<div class="container-fluid">
		<div class="row leadership-ct approaches-ct lint-h">
			<div class="col-md-6">
				<p class="p-0"><strong>Operating and academic parterships:</strong></p>
				<h2>Your chance to shape the<br>
nation through education.</h2>
				<!-- <h5>to create world leading education.</h5> -->
				<p>From ready intellectual capital to ready expertise across fields, we have created the whole universe of quality education. So you can simply replicate an already successful school model, avoid the never-ending process of trial and error, and save time and money.</p>
				<p>Even though we’re inflexible when it comes to delivering quality education, we’re quite flexible in the ways we go about our partnerships. From branding options to product types, you can choose a format that works best in achieving your goals.</p>
			</div>
			<div class="col-md-6 col">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/partnership-ban.jpg" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row table-ct">
			<h3>Brand & Board Range</h3>
			<div class="table-responsive">
				<table class="table bgcolor">
				  <thead>
				    <tr>
				      <th scope="col" class="wt-50">Name</th>
				      <th scope="col" class="wt-50">Type</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">The Green Acres Academy</th>
				      <td>Full day premium ICSE / CBSE school</td>
				    </tr>
				    <tr>
				      <th scope="row">The Green Acres Academy Horizons (Only from 2023)</th>
				      <td>Double shift ICSE / CBSE school</td>
				    </tr>
				    <tr>
				      <th scope="row">The Seven Rivers International School (Only from 2023)</th>
				      <td>Full day premium IGCSE school</td>
				    </tr>
				    <tr>
				      <th scope="row">The Green Acres Vidya Bhavan (Only from 2023)</th>
				      <td>Low cost, multi-shift school</td>
				    </tr>
				  </tbody>
				</table>     
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row table-ct">
			<h3>Branding Options</h3>
			<div class="table-responsive">
				<table class="table bgcolor">
				  <thead>
				    <tr>
				      <th scope="col" class="wt-33">Type</th>
				      <th scope="col" class="wt-33">Description</th>
				      <th scope="col" class="wt-33">Example</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">Acres Foundation Co-brand</th>
				      <td>Acres Foundation main Brand with Owner co-branded</td>
				      <td>The Green Acres Academy - Smt S Desai Educational Trust
				      	<br>SM Tech - Seven Rivers International School</td>
				    </tr>
				    <tr>
				      <th scope="row">Acres Foundation Sub-brand</th>
				      <td>Owner’s name primary with Acres <br>Foundation as sub-brand</td>
				      <td><strong>SM Desai School</strong><br>
by The Acres Foundation<br>
<strong>Mehta Tech & Co International School</strong><br>
by The Acres Foundation</td>
				    </tr>
				  </tbody>
				</table>     
			</div>
			<h5>*Note: No difference in brand fees between the options.</h5>
		</div>
	</div>
</section>


<section>
	<div class="container">
		<div class="row table-ct">
			<h3>Types of Agreements</h3>
			<div class="table-responsive">
				<table class="table bgcolor lft-bg-color">
				  <thead>
				    <tr>
				      <th scope="col" class="wt-22">Type</th>
				      <th scope="col" class="wt-32">Provided by Acres Foundation</th>
				      <th scope="col" class="wt-20">Fees</th>
				      <th scope="col" class="wt-26">Provided by Owner Trust</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">Brand & Academic <br>Partnership + <br>Management</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Brand</li>
				      		<li>Set-up materials and support</li>
				      		<li>Annual Educational Training, Administration, Marketing, Budgeting & Quality Assurance materials</li>
				      		<li>All day-to-day management of the school (monthly reporting to Owner Trust)</li>
				      		<li>Continuous Quality Improvement support</li>
				      	</ul>
				      </td>
				      <td>
						<p>Academic Partnership Set-up fee</p>
						<p>Annual Brand & Academics fee</p>
						<p>Annual Management fee</p>
				      </td>
				      <td>
				      	<ul class="pl-4">
				      		<li>Building & Infrastructure</li>
				      		<li>Revenue Collection & Payments</li>
				      	</ul>
				      </td>
				    </tr>

				    <tr>
				      <th scope="row">Brand & Academic<br>Partnership</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Brand</li>
				      		<li>Set-up materials and support</li>
				      		<li>Annual Educational Training, Administration, Marketing, Budgeting & Quality Assurance materials</li>
				      		<li>Continuous Quality Improvement support</li>
				      	</ul>
				      </td>
				      <td>
						<p>Academic Partnership Set-up fee</p>
						<p>Annual Brand & Academics fee</p>
				      </td>
				      <td>
				      	<ul class="pl-4">
				      		<li>Building & Infrastructure</li>
				      		<li>Revenue Collection & Payments</li>
				      		<li>All day-to-day management of the school</li>
				      	</ul>
				      </td>
				    </tr>

				    <tr>
				      <th scope="row">Brand only</th>
				      <td colspan="3">
				      	<p>Not offered by AF in order to maintain quality standards</p>
				      </td>
				    </tr>
				  </tbody>
				</table>     
			</div>
		</div>
	</div>
</section>




<section>
	<div class="container">
		<div class="row table-ct">
			<h3>Details of Service Provided</h3>
			<div class="table-responsive">
				<table class="table bgcolor lft-bg-color">
				  <thead>
				    <tr>
				      <th scope="col" class="wt-20 white-bg-th"></th>
				      <th scope="col" colspan="2" class="wt-28 red-bg-th">If Brand + Academic Partnership</th>
				      <th scope="col" rowspan="2" class="wt-20 red-bg-th">+ Management <Br>Agreement</th>
				    </tr>
				    <tr>
				      <th scope="col" class="wt-20">Item</th>
				      <th scope="col" class="wt-28">Education Partner Role</th>
				      <th scope="col" class="wt-32">Acres Foundation Role -
<br>Brand + Academic Partner</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row">Brand</th>
				      <td>
				      	
				      </td>
				      <td>
						License to use the TGAA brand.
				      </td>
				      <td>
				      	No change
				      </td>
				    </tr>

				    <tr>
				      <th scope="row">Administration</th>
				      <td>
				      	Running / Administration of the school.
				      </td>
				      <td>
						<strong>“School-in-a-box” :</strong>
						<ul class="pl-4">
							<li>All SOPs and formats for smooth operations and management.</li>
							<li>Training protocols and materials for following SOPs.</li>
						</ul>
				      </td>
				      <td>
				      	Acres Foundation does all
				      </td>
				    </tr>


				    <tr>
				      <th scope="row">Admissions & <br>Marketing</th>
				      <td>
				      	Managing Marketing and <br>Admissions to the school.
				      </td>
				      <td>
				      	<ul class="pl-4">
							<li>Brochures & Collaterals</li>
							<li>Admissions team sales training</li>
							<li>Digital Marketing Execution (optional, additional cost)</li>
						</ul>
				      </td>
				      <td>
				      	Acres Foundation does all
				      </td>
				    </tr>

				    <tr>
				      <th scope="row">Financial <br>Management</th>
				      <td>
				      	All Revenues, Payments and <br>Finances of the school.
				      </td>
				      <td>
						<ul class="pl-4">
							<li>Budget formats, guidelines, and SOP</li>
							<li>Leadership co-selection, Training and Development</li>
						</ul>
				      </td>
				      <td>
				      	No change
				      </td>
				    </tr>


				    <tr>
				      <th scope="row">Leadership</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Leadership co-selection.</li>
				      		<li>Day-to-day oversight and mentoring of leaders.</li>
				      		<li>Problem solving.</li>
				      	</ul>
				      </td>
				      <td>
						<ul class="pl-4">
							<li>Leadership co-selection.</li>
							<li>Leadership Training and Development.</li>
							<li>Membership in system wide NLCs.</li>
							<li>Ongoing mentoring & support.</li>
						</ul>
				      </td>
				      <td>
				      	Acres Foundation does all
				      </td>
				    </tr>


				    <tr>
				      <th scope="row">Teacher <br>Recruitment & <br>Training</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Selection of teachers</li>
				      		<li>Mentoring and overall leadership of staff</li>
				      	</ul>
				      </td>
				      <td>
						<ul class="pl-4">
				      		<li>Recruitment support - Sourcing CVs & Interview and testing protocols.</li>
				      		<li>Teacher Training and Development (Fortnightly).</li>
				      		<li>Teacher membership in system wide Professional Learning Communities (PLCs).</li>
				      		<li>Performance Management System (PMS).</li>
				      		<li>Personal Improvement Program (PiP).</li>
				      		<li>Teacher load allocation guide.</li>
				      	</ul>
				      </td>
				      <td>
				      	Acres Foundation does all
				      </td>
				    </tr>


				    <tr>
				      <th scope="row">Curriculum</th>
				      <td>
				      	
				      </td>
				      <td>
						<strong>Curriculum map:</strong>
						<ul class="pl-4">
							<li>Learning objectives for each grade level and every subject.</li>
							<li>Sample assessments/tests for the same.</li>
							<li>Sample learning experiences for the same.</li>
							<li>Suggested teaching resources for the same.</li>
							<li>Grade wise period allocation guide and sample time tables & calendars schedules.</li>
							<li>Lesson planning protocol.</li>
							<li>Blended/ Online content.</li>
						</ul>
						<strong>AF Proprietary Content:</strong>
						<ul class="pl-4">
							<li>LEAPED (Leadership, Ethics and Awareness of Personal & Emotional Development).</li>
							<li>Home Room Program.</li>
							<li>Citizenship program.</li>
							<li>Test Series.</li>
						</ul>
				      </td>
				      <td>
				      	Acres Foundation does all
				      </td>
				    </tr>



				    <tr>
				      <th scope="row">Socio-Emotional <br>Climate & Equity</th>
				      <td>
				      	
				      </td>
				      <td>
						<ul class="pl-4">
							<li>Multi-tier System of Supports (MTSS) Program - to support inclusion and learning differences</li>
							<li>Positive Behavioural Interventions and Supports (PBIS) Program - to support whole school behaviour management.</li>
							<li>Special Educational Needs & Counselling department protocols.</li>
						</ul>
				      </td>
				      <td>
				      	
				      </td>
				    </tr>



				    <tr>
				      <th scope="row">Parent <br>Engagement</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Public face of the school.</li>
				      		<li>Management of parent relationships</li>
				      	</ul>
				      </td>
				      <td>
						<ul class="pl-4">
							<li>SOPs for PTA, Committees</li>
							<li>Plan and SOP for parent engagement.</li>
						</ul>
				      </td>
				      <td>
				      	Shared
				      </td>
				    </tr>



				    <tr>
				      <th scope="row">Infrastructure</th>
				      <td>
				      	<ul class="pl-4">
				      		<li>Building or renting the required infrastructure.</li>
				      		<li>Managing the infrastructure</li>
				      	</ul>
				      </td>
				      <td>
						<ul class="pl-4">
				      		<li>School & Infrastructure Design Handbook.</li>
				      	</ul>
				      </td>
				      <td>
				      	No change
				      </td>
				    </tr>


				    <tr>
				      <th scope="row">Continuous <br>Improvement & Audity</th>
				      <td>
				      	Term wise milestone meetings to discuss audit findings and suggested strategy.
				      </td>
				      <td>
						<ul>
							<li>Continuous improvement audit format and findings.</li>
							<li>Data dashboard for owner to track school performance.</li>
						</ul>
				      </td>
				      <td>
				      	No change
				      </td>
				    </tr>

				  </tbody>
				</table>     
			</div>
		</div>
	</div>
</section>





<section>
	<div class="container">
		<div class="row">
			<div class="col part-ct">
				<h3>Types of Agreements</h3>
				<p>- Brand partnership that’s compliant to your requirements.</p>
				<p>- Access to our sales training resources.</p>
				<p>- Access to our digital marketing team and all digital brand building resources.</p>
			</div>

		</div>
		<div class="row">
			<div class="col rd-part-afrem">
				<h2>Partnership Agreement</h2>
				<p>Together, let’s build a better school. And a better tomorrow. Given below is the process flow for signing of Education Partners:</p>

				<h3>A) Preliminary Stage</h3>
				<p> -  Send your interest to us via email on ep.enquiries@acresfoundation.org, call our Partnership Manager  at +91 98200 30995 or fill up this form https://bit.ly/AF-EPInquiry and we will contact you.<br>
-   You will receive a revert call from our team member to confirm your needs and requirements (within 2 working days).<br>
-   If your needs meet our services, a detailed Education Partner (EP) Proposal will be shared with you immediately.<br>
-   Video conferences, conference calls, or in-person meetings will be scheduled to discuss the proposal in detail, answer any questions and understand your needs.</p>

<h3>B) Term Sheet Stage</h3>
<p> - The Acres Foundation will conduct Basic Due Diligence on the operations, local market and existing market opportunities, to decide whether the project is feasible and a match for the brand (1-2 weeks).<Br>
-  If Basic Due Diligence is positive, a Term Sheet will be signed. Advance payment of the fees will be due at this time.</p>

<h3>C) Due Diligence & Final Agreement Stage</h3>
<p>-  Detailed Due Diligence to be conducted by the parties to ensure a complete understanding of needs, timelines, market, costs, prices, challenges, etc. This will occur over a series of meetings or visits (2-4 weeks).<br>
-  Signing of the final agreement with payment of balance sign-up fees..</p>

<h3>D) Pre-launch Planning Stage <span>(9 months before the academic term begins)</span></h3>
<p class="p-0">-  Education Partner (EP) will appoint a Single Point Of Contact (SPOC) person to connect with our Single Point Education Expert (SPEE).<br>
-  Planning phase commences. Ideally, minimum 6 months prior to the start of an academic term. Here, all plans will be put together including:</p>
<div class="ulist">
	<ul>
		<li>-  Budgets</li>
		<li>-  Recruitment</li>
		<li>-  Training and Development</li>
		<li>-  Protocols, Policies, and SOPs</li>
	</ul>

	<ul>
		<li>-  Audits</li>
		<li>-  Admissions</li>
		<li>-  Curriculum mapping</li>
		<li>-  Material selection</li>
	</ul>
</div>
<p>- Admissions and recruitment support will launch 6 months before the academic term begins.</p>

<h3>E)  Launch Stage <span>(2 months before the academic term begins)</span></h3>
<p>-  Execution commences with leader and teacher workshops 1-2 months before the academic term begins.</p>

<h3>F)  Ongoing</h3>
<p>-  Continuous Improvement will occur term to term to ensure that the school is able to meet quality goals.<br>
-  Preset Monthly, Quarterly, and Termwise meetings with various members of the Acres Foundation team will occur for support, reviews, implementation, and problem solving.<br>
-  Ongoing mentoring and support from our SPEE.</p>
			</div>
		</div>
	</div>
</section>



<section>
	<div class="container">
		<div class="row table-ct">
			<h3>Financial Terms</h3>
			<div class="table-responsive">
				<table class="table bgcolor lft-bg-color">
				  <thead>
				    <tr>
				      <th scope="col" class="wt-10 text-center">Sr. No.</th>
				      <th scope="col" class="wt-40">Terms</th>
				      <th scope="col" class="wt-50">Offering by Acres Foundation</th>
				    </tr>
				  </thead>
				  <tbody>
				    <tr>
				      <th scope="row" class="text-center">1</th>
				      <td>
				      	Number of years of Agreement
				      </td>
				      <td>
						30 years
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">2</th>
				      <td>
				      	One Time Education Partner Sign-up Fee
				      </td>
				      <td>
						<ul class="pl-4">
							<li>20 lakhs + GST for Brand & Academics.</li>
							<li>10 lakhs + GST for Operations & Management.</li>
							<li>Rs. 7.5 lakhs payable at the time of signing this term sheet. Balance amount payable at the time of signing Education Partnership Agreement.</li>
						</ul>
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">3</th>
				      <td>
				      	Annual Brand & Academics + <br>Operations & Management Fees
				      </td>
				      <td>
						Higher of the Minimum Guarantee* or Percentage of Gross Revenue plus GST
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">3.1</th>
				      <td>
				      	*Minimum Guarantee
				      </td>
				      <td>
						Rs. 15 Lakhs per annum payable in advance on quarterly basis on or before the 7th day of commencement of the quarter.
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">3.2</th>
				      <td>
				      	<strong>Percentage of Revenue</strong>
				      	<br>
				      	<div class="float-right">
				      		<strong>Student Count</strong>
				      		<ul class="pl-4">
				      			<li>up to 500</li>
				      			<li>501 to 1000</li>
				      			<li>1000 to 1500</li>
				      			<li>1500 to 2000</li>
				      			<li>2000+</li>
				      		</ul>
				      	</div>
				      </td>
				      <td>
						This will be based on the total count of students as under:
						<br>
						<strong>Percentage of Gross Revenue for Brand & Academics + Operations & Management</strong>
						<ul class="pl-4">
							<li>10% + 5%</li>
							<li>9% + 5%</li>
							<li>8% + 5%</li>
							<li>7% + 5%</li>
							<li>6% + 5%</li>
						</ul>
						+ GST as applicable
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">4</th>
				      <td>
				      	Definition of Gross Revenue
				      </td>
				      <td>
						Gross Revenue considered for agreement will consist of the following:
						<ul class="pl-4">
							<li>Admission Fees</li>
							<li>Tuition Fees</li>
							<li>Term Fees</li>
						</ul>
				      </td>
				    </tr>


				    <tr>
				      <th scope="row" class="text-center">5</th>
				      <td>
				      	Affiliation Board
				      </td>
				      <td>
						CBSE  /  ICSE  /  IGCSE
				      </td>
				    </tr>

				  </tbody>
				</table>     
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>