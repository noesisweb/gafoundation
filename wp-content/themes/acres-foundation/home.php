<?php /* Template Name: Home page */ ?>
<?php get_header(); ?>
<section class="max-width-ct">
	<div class="container-fluid">
		<div class="row">
			<div class="col top-slider">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/banner.jpg" alt="" class="img-fluid">
				<div class="sty-ribban">
					<h2>The Acres Foundation.</h2>
					<p>Shaping the future of Indian education.</p>
				</div>
				<div class="i-button">
					<a href="#popup1">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/i.png" alt="" class="img-fluid"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="comman-cls">
	<div class="container">
		<div class="row new-idea">
			<div class="col-md-6">
				<h5>Meeting the challenges of tomorrow with</h5>
				<h2>new ideas in education.</h2>
				<p>At Acres Foundation, we bring together the finest educators to chalk out a new direction for education in India.</p>
<p>Creating educational programs and institutes that are of the highest standards but yet remain accessible and inclusive to all; we aim to bring a transformative force in the society that helps shape the future of the country.
</p>
			</div>
			<div class="col-md-6">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/newidea.jpg" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<section class="comman-cls education-ct">
	<h5>The four pillars that make us</h5>
	<h2>India's leading educationists.</h2>
	<div class="container">
		<div class="row">
			<div class="col col-md-3">
					<h3>Intellectual <br>capital</h3>
					<p>We have some of the finest minds, capable of creating new paradigms in education.</p>
			</div>

			<div class="col col-md-3">
				<h3>Idea to <Br>implementation</h3>
				<p>We are a think-tank. But we also know how to move quickly to turn ideas into reality.</p>
			</div>

			<div class="col col-md-3">
				<h3>Progressive <br>solutions</h3>
				<p>We are not satisfied with the status quo. We desire change – to be inspired by the best ideas, and to create proven advances in education.</p>
			</div>

			<div class="col col-md-3">
				<h3>Synthesising &<Br>building</h3>
				<p>We build ideas. We build schools. We are building a new future for thousands of children across India.</p>
			</div>
		</div>
	</div>
</section>

<section class="vision-ct">
	<h2>Our vision</h2>
	<div class="blockqute-ct">
		<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blolqu.svg" alt="" class="img-fluid">
		<p>To create changemakers across India by providing a powerful and accessible 21st century education.</p>
		<img src="<?php echo get_template_directory_uri()  ?>/assets/images/blolqu1.svg" alt="" class="img-fluid">
	</div>
</section>

<section class="comman-cls work-ct">
	<h5>We've been recognised for</h5>
	<h2>our groundbreaking work.</h2>
	<h4>Constantly evolving and changing the landscape of Indian education, we’ve gone the extra mile to make a difference. In a short span of 10 years, we’ve instituted 4 schools with groundbreaking educational approaches, and garnered many awards and accolades.</h4>

	<div class="container">
		<div class="row">
			<div class="bg-path col-md-4">
				<h2>Eldrok India,<br>K-12 Awards, 2019</h2>
				<p>Excellence in Arts and Sports</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Eldrok India,<br>K-12 Awards, 2019</h2>
				<p>Excellence in Arts and Sports</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>EducationWorld<br>Grand Jury Awards, 2017</h2>
				<p>TGAA Chembur was ranked</p>
				<p>Top 5 in India in ‘New</p>
				<p>Technologies Used’</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Hindustan Times, 2019</h2>
				<p>Ranked No. 10 in</p>
				<p>Mumbai East Zone</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Times School Survey, 2018</h2>
				<p>Fastest Emerging National</p>
				<p>Curriculum School</p>
				<p>in Mumbai</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Times School Survey, 2015</h2>
				<p>TGAA Chembur and</p>
				<p>Mulund named finalists</p>
				<p>among the Top Emerging</p>
				<p>Schools in Mumbai</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Rocksport, 2018</h2>
				<p>Inspiring Educator of</p>
				<p>Maharashtra awarded to</p>
				<p>Principal Lakshminarayanan</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Rotary Club of<br>Mumbai Metropolitan, 2017</h2>
				<p>Principal Manju Mehta presented</p>
				<p>with a vocational  award for</p>
				<p>‘Outstanding work in the field</p>
				<p>of School Education’</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>World Education<br>Congress, 2016</h2>
				<p>Principal Ms. Manju Mehta</p>
				<p>ranked one of the</p>
				<p>‘Most Influential Principals</p>
				<p>of Indian Schools’</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>Asia Fest Award, 2015</h2>
				<p>Best Education Design</p>
			</div>

			<div class="bg-path col-md-4">
				<h2>BW Education</h2>
				<p>TGAA Brand received</p>
				<p>40 under 40</p>
				<p>Club of Achievers</p>
			</div>
		</div>
	</div>
</section>

<section class="comman-cls">
	<div class="container">
		<div class="row new-idea footer-idea">
			<div class="col-md-6">
				<h5>The Green Acres Academy &<br>Seven Rivers International School.</h5>
				<h2>leading The way.</h2>
				<p>Bringing together the best of Indian and International education, The Acres Foundation is the think tank behind Mumbai’s top emerging school The Green Acres Academy (ICSE/CBSE) and the highly acclaimed Seven Rivers International School (IGCSE).</p>
			</div>
			<div class="col-md-6">
				<img src="<?php echo get_template_directory_uri()  ?>/assets/images/logo-seven.jpg" alt="" class="img-fluid">
			</div>
		</div>
	</div>
</section>

<div id="popup1" class="overlay">
	<div class="popup">
		<h2>Get updates from<br>The Acres Foundation</h2>
		<p>Enter your details below</p>
		<a class="close" href="#">&times;</a>
		<div class="content">
			<!-- Thank to pop me out of that button, but now i'm done so you can close this window. -->
		  	<?php  gravity_form( 1, false, false, false, '', true, 12 );?>
		</div>
	</div>
</div>
<style type="text/css">




.button:hover {
  /*background: #06D85F;*/
}


.overlay:target {
 /* visibility: visible;
  opacity: 1;*/
}

.popup .close:hover {
  /*color: #06D85F;*/
}
</style>
<?php get_footer(); ?>