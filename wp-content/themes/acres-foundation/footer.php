<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Acres_Foundation
 */

?>

	
<footer>
	<section class="footer-one">
		<div class="container">
			<div class="row">
				<div class="col-md-4 first-row">
					<a class="navbar-brand" href="<?php echo site_url(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri()  ?>/assets/images/footer-logo.svg" alt=""></a>

					<ul>
						<li><a href="">HOME</a></li>
						<li><a href="">LEADERSHIP</a></li>
						<li><a href="">APPROACH</a></li>
						<li><a href="">PARTNERSHIPS</a></li>
						<li><a href="">BLOG</a></li>
					</ul>
				</div>
				<div class="col-md-4 second-row">
					<h2>CONNECT</h2>
					<div class="icon-ct">
							<a href=""><img src="<?php echo get_template_directory_uri()  ?>/assets/images/fb.svg"></a>
							<a href=""><img src="<?php echo get_template_directory_uri()  ?>/assets/images/inst.svg"></a>
							<a href=""><img src="<?php echo get_template_directory_uri()  ?>/assets/images/in.svg"></a>
						</div>
				</div>
				<div class="col-md-4 third-row">
					<h2>CONTACT</h2>
					<p>Mr. Aaditya Lohana</p>
					<p>ep.enquiries@acresfoundation.org</p>
					<p><a href="tel:+919820030995">+91 98200 30995</a></p>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row coppy-right">
				<div class="col-md-4">
					<p>© 2020 THE ACRES FOUNDATION</p>
				</div>
				<div class="col-md-4 second-row">
					<a href="">Privacy Policy</a>
				</div>
				<div class="col-md-4">
					<p>Design and content: Spike Brand & Content Studio</p>
				</div>
			</div>
		</div>
	</section>
</footer>

<?php wp_footer(); ?>

</body>
</html>
